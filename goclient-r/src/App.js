import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Products from './Components/Products';
import FindProducts from './Components/FindProducts';
import FormProducts from './Components/FormProducts';
import './App.scss';

function App() {
  	return (
		<Router>
			<div>
				<nav>
					<ul>
						<li>
							<Link to="/">GoClient</Link>
						</li>
						<li>
							<Link to="/buscar">
								<figure>
									<img src={process.env.PUBLIC_URL + "/search.svg"} alt="" />
								</figure>
							</Link>
						</li>
					</ul>
				</nav>

				<Switch>
					<Route path="/" exact component={Products} />
					<Route path="/buscar" component={FindProducts} />
					<Route path="/agregar" component={FormProducts} />
					<Route path="/agregar/:id" component={FormProducts} />
				</Switch>
			</div>
		</Router>
  	);
}

export default App;

import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Page from './page';

class Products extends Component {
    constructor(props){
        super(props);

        this.state = {
            Products : []
        }

        this.clickDelete = this.clickDelete.bind(this);
        this.loadData = this.loadData.bind(this);
    }
    loadData(){
        fetch("/product")
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            this.setState({Products: [...data] })
        });
    }

    componentDidMount(){
        this.loadData();
    }

    clickDelete(e){
        fetch(`/product/${e.target.id}`, {
            method: 'DELETE',
        }).then(res => res.status === 200 ? alert("Eliminado Correctamente"): alert("Error al eliminar..."))
        .then(() => this.loadData())
    }
    render() {
        const {Products} = this.state;
        return (
            <section>
                <article style={{textAlign: "center"}}>
                    <h2>Productos mas vendidos</h2>
                </article>
                <Link to={`/agregar`} style={{textDecoration: "none", fontSize: "1.2em", color: "#2c3e50", display: "grid"}}>
                    <button type="button" style={{gridColumn: "1fr"}}>Nuevo</button>
                </Link> 
                <Page products={Products} clickDelete={this.clickDelete}/>
            </section>
        )
    }
}

export default Products;
import React, {Fragment} from 'react';
import { Link } from "react-router-dom";
import './style.scss';

const Page = (props) => {
    return (
        <Fragment>
            <ul className="list">
                {props.products.map(item => 
                <li key={item.productId} className="list-item">
                    <span>{item.name}</span>
                    <span>Price: {item.price} $</span>
                    <p style={{float: "right"}}>
                        <Link to={`/agregar/${item.productId}`}>Edit</Link> | <span onClick={props.clickDelete} id={item.productId}>Eliminar</span>
                    </p>
                </li>)}
            </ul>
        </Fragment>
    )
}

export default Page;
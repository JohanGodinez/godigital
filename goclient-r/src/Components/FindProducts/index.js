import React, {Component} from 'react';
import Page from '../Products/page';

class FindProducts extends Component {
    constructor(props){
        super(props);

        this.state = {
            Products : [],
            findText : "",
            Filter: []
        }
        
        this.onChangeFind = this.onChangeFind.bind(this);
    }
    componentDidMount(){
        fetch("/product")
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            this.setState({Products: [...data] })
        });
    }
    onChangeFind(e) {
        let {Products, Filter} = this.state
        Filter = Products.filter(item => String(item.name.toUpperCase()).includes(e.target.value.toUpperCase()) || String(item.price).search(e.target.value) >= 0 )
        this.setState({findText: e.target.value, Filter})
    }

    render() {
        const {findText, Filter} = this.state;
        return (
            <section>
                <div className="flex-container">
                    <input style={{flexGrow: 1}} type="text" placeholder="Buscar" value={findText} onChange={this.onChangeFind} />
                </div>
                <Page products={Filter}/>
            </section>
        )
    }
}

export default FindProducts;
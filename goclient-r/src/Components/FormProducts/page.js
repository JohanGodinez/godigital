import React, {Fragment} from 'react';
import './style.scss';

const Page = (props) => {
    return (
        <Fragment>
            <form className="form">
                <div className="form-group">
                    <input type="text" placeholder="Nombre" value={props.name} onChange={props.changeName}/>
                </div>
                <div className="form-group">
                    <input type="text" placeholder="Precio" value={props.price} onChange={props.changePrice}/>
                </div>
                <div className="form-group">
                    <input type="text" placeholder="Unidad" value={props.unit} onChange={props.changeUnit}/>
                </div>
                <div className="form-group">
                    <select onChange={props.changeCategory} value={props.categoryId}>
                        {props.categories.map(item => <option key={item.categoryId} value={item.categoryId}>{item.name}</option>)}     
                    </select>
                </div>
                <button type="button" onClick={props.save}>Guardar</button>
            </form>
        </Fragment>
    )
}

export default Page;
import React, {Component} from 'react';
import Page from './page';

class FormProducts extends Component {
    constructor(props){
        super(props);

        this.state = {
            product : {},
            name : "",
            price: "",
            unit: "",
            categoryId : 0,
            categories: [],
            productId : 0
        }

        this.changeName = this.changeName.bind(this);
        this.changePrice = this.changePrice.bind(this);
        this.changeUnit = this.changeUnit.bind(this);
        this.changeCategory = this.changeCategory.bind(this);
        this.save = this.save.bind(this);
        this.textClean = this.textClean.bind(this);
    }
    textClean(){
        this.setState({
            product : {},
            name : "",
            price: "",
            unit: "",
            categoryId : 0,
            productId : 0
        })
    }
    componentDidMount(){
        let id = this.props.location.pathname.replace(`${this.props.match.path}/`, '');
        
        if(!isNaN(id)){
            fetch(`/product/${id}`)
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                let {name, price, unit, categoryId} = data
                this.setState({name, price, unit, categoryId, productId : id})
            });
        }

        fetch("/category")
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            this.setState({categories: [...data] })
        });
    }

    changeName(e) {
        this.setState({ name: e.target.value})
    }

    changePrice(e){
        this.setState({ price: e.target.value})
    }

    changeUnit(e){
        this.setState({ unit: e.target.value})
    }

    changeCategory(e){
        this.setState({ categoryId: e.target.value})
    }

    save(){
        let {product, name, price, unit, categoryId, productId} = this.state;
        product = {...product, name, price, unit, categoryId};
        
        if(productId === 0){
            fetch("/product", {
                method: 'POST',
                body: JSON.stringify(product),
                headers:{
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.status === 200 ? alert("Guardado Correctamente"): alert("Error al guardar..."))
        }else {
            fetch(`/product/${productId}`, {
                method: 'PUT',
                body: JSON.stringify(product),
                headers:{
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.status === 200 ? alert("Editado Correctamente"): alert("Error al editar..."))
        }
        this.textClean()
    }

    render() {
        const {name, price, unit, categoryId, categories} = this.state
        return (
            <section>
                <Page 
                    changeName={this.changeName} 
                    changePrice={this.changePrice} 
                    changeUnit={this.changeUnit} 
                    changeCategory={this.changeCategory}
                    save={this.save}
                    name={name}
                    price={price}
                    unit={unit} 
                    categoryId={categoryId}
                    categories={categories}
                    />
            </section>
        )
    }
}

export default FormProducts;
# GODIGITAL

Proyecto llevado a cabo bajo la tecnología .Net Core para desarrollar la API que servirá los datos que serán consumidos por el cliente desarrollado en React

```Ne se configuro un boton para regresar o canclear. se puede usar el titlo para ambas acciones ```

## API
Para poder ejecutar el api debe de correr las migraciones con los camandos correspondientes 

se recomienda realizar un restore de los paquetes

```
dotnet restore
```

y luego proceder con la DB

```
dotnet ef migrations add [Nombre]
dotnet ef database update
```


## Cliente
El cliente como fue descrito anteriormente, fue desarrollado en React, esta separado por componentes, que comparten funcionalidades entre pantalla, se uso Sass para dar estilos a las vistas y React-Router para el redireccionamiento.

primero se tiene que instalar las librerias
```
npm i
```
Esto leera el archvo package.json e instala las librerias necesarias.

Una vez finalizada la instalacion, basta con entrar en la carpeta `goclient-r` y teclear el siguietne comando.

```
npm start 
```
 
 Dentro del archivo Packga.json se encuentra configurado el proxy que es la direccion del WepAPI 

## Capturas
https://drive.google.com/open?id=19Hp_U4eyAU_dogrI-XZmLet88LMkk23r
https://drive.google.com/open?id=1nr_gvHCFSSwx2IjGQtmAk4ULtN-CDHVL
https://drive.google.com/open?id=1yutXyiRils75ufH2WvW6zLIPKKlVP5ss
https://drive.google.com/open?id=1bJ3s9qQYagb0v1VnjdCTQWF9vt_yLxwb





Project carried out under the .Net Core technology to develop the API that will serve the data that will be consumed by the client developed in React

## API
In order to execute the api you must run the migrations with the corresponding camanders

it is recommended to restore the packages

``
dotnet restore
``

and then proceed with the DB

``
dotnet ef migrations add [Name]
dotnet ef database update
``


## Client
The client as described above, was developed in React, it is separated by components, which share functionalities between the screen, Sass was used to give styles to the views and React-Router for redirection.

first you have to install the libraries
``
npm i
``
This will read the package.json file and install the necessary libraries.

Once the installation is finished, simply enter the `goclient-r` folder and type the following command.

``
npm start
``
 
 Inside the Packga.json file the proxy is configured, which is the WepAPI address


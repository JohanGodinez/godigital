﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace GoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly GoAPIDBContext _context;

        public CategoryController(GoAPIDBContext context)
        {
            _context = context;

            if (_context.Categories.Count() == 0)
            {
                _context.Categories.Add(new Category { Name = "Category 1", Description = "First Category" });
                _context.Categories.Add(new Category { Name = "Category 2", Description = "Second Category" });
                _context.SaveChanges();
            }
        }

        [HttpGet]
        public ActionResult<IEnumerable<Category>> Get()
        {
            return _context.Categories.ToList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<Category> Get(int id)
        {
            return _context.Categories.Where(a => a.CategoryId == id).FirstOrDefault();
        }

        // POST api/values
        [HttpPost]
        public string Post(Category category)
        {
            if (ModelState.IsValid)
            {
                _context.Categories.Add(category);
                _context.SaveChanges();

                return "Save";
            }

            return "Not valid format";
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public string Put(int id, Category category)
        {
            if (ModelState.IsValid)
            {
                Category cat = _context.Categories.Where(a => a.CategoryId == id).FirstOrDefault();
                cat.Name = category.Name;
                cat.Description = category.Description;
                _context.SaveChanges();

                return "Save";
            }
            return "Not valid format";
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public string Delete(int id)
        {
            if (id != 0)
            {
                Category category = _context.Categories.Where(a => a.CategoryId == id).FirstOrDefault();
                if (category != null)
                {
                    _context.Categories.Remove(category);
                    _context.SaveChanges();
                    return "Save";
                }
                return "Id does not exist";

            }
            return "Not valid Id";
        }
    }
}

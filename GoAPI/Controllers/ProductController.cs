using System.Collections.Generic;
using System.Linq;
using GoAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace GoAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly GoAPIDBContext _context;

        public ProductController(GoAPIDBContext context)
        {
            _context = context;

            if (_context.Products.Count() == 0)
            {
                _context.Products.Add(new Product { Name = "Product 1", Unit = "Unit", Price = 20.15, CategoryId = 1 });
                _context.SaveChanges();
            }
        }

        [HttpGet]
        public ActionResult<IEnumerable<Product>> Get()
        {
            return _context.Products.ToList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<Product> Get(int id)
        {
            return _context.Products.Where(a => a.ProductId == id).FirstOrDefault();
        }

        // POST api/values
        [HttpPost]
        public string Post(Product product)
        {
            if (ModelState.IsValid)
            {
                _context.Products.Add(product);
                _context.SaveChanges();

                return "Save";
            }

            return "Not valid format";
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public string Put(int id, Product product)
        {
            if (ModelState.IsValid)
            {
                Product prod = _context.Products.Where(a => a.ProductId == id).FirstOrDefault();
                prod.Name = product.Name;
                prod.Price = product.Price;
                prod.Unit = product.Unit;
                prod.CategoryId = product.CategoryId;
                _context.SaveChanges();

                return "Save";
            }
            return "Not valid format";
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public string Delete(int id)
        {
            if (id != 0)
            {
                Product product = _context.Products.Where(a => a.ProductId == id).FirstOrDefault();
                if (product != null)
                {
                    _context.Products.Remove(product);
                    _context.SaveChanges();
                    return "Save";
                }
                return "Id does not exist";

            }
            return "Not valid Id";
        }
    }
}
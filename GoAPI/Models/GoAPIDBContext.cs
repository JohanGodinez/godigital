using Microsoft.EntityFrameworkCore;

namespace GoAPI.Models
{
    public class GoAPIDBContext : DbContext
    {
        public GoAPIDBContext(DbContextOptions<GoAPIDBContext> options)
            : base(options)
        {
        }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GoAPI.Models
{ 
    public class Product
    {
        public Product()
        {
            this.Category = new HashSet<Category>();
        } 
        
        [Key]
        public int ProductId { get; set; }
        [Required]
        public string Name { get; set; }
        public double Price { get; set; }
        public string Unit { get; set; }
        public string Image {get; set;}
        public int CategoryId { get; set; }
        protected virtual ICollection<Category> Category { get; set; }
    }
}